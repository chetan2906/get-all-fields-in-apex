public with sharing class ApexUtility {
    
    
    /*
*	@methodName = getAllFields
*	@return = Set of field Names
*	@params = object name (both custom and standard)
*	@description = Method takes object name as an argument and return all (both custom and standard) fields
*/
    global static Set<String> getAllFields(String objectName){
        Map<String, Schema.SObjectType> completeSchema = Schema.getGlobalDescribe(); 
        Schema.SObjectType requiredObject = completeSchema.get(objectName);
        return requiredObject.getDescribe().fields.getMap().keySet();
    }
    
    /*
*	@methodName = getAllFields
*	@return = Map of object names and their field Names
*	@params = List of object name (both custom and standard)
*	@description = Method takes object name as an argument and return Map of object name and their (both custom and standard) fields
*/
    global static Map<String, Set<String>> getAllFields(List<String> objectNames){
        Map<String, Schema.SObjectType> completeSchema = Schema.getGlobalDescribe(); 
        Map<String, Set<String>> objectVsFieldMap = new Map<String, Set<String>>();
        for(String objectName : objectNames){
            Schema.SObjectType requiredObject = completeSchema.get(objectName);
            objectVsFieldMap.put(objectName,requiredObject.getDescribe().fields.getMap().keySet());
        }
        return objectVsFieldMap;
    }
}
